import { resolve } from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite'
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers'
import { AndDesignVueResolve, createStyleImportPlugin } from 'vite-plugin-style-import'


// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    Components({
      resolvers: [AntDesignVueResolver()],
    }),
    createStyleImportPlugin({
      resolves: [AndDesignVueResolve()],
    }),
  ],
  server: {
      port: 8080,
      host: true,
      open: true,
      proxy: {
        '/api': {
          // target: 'https://f2e.dxy.net/mock-api/client/627b611bb7195b0a1d4e9085',
          target: 'http://localhost:8080/',
          changeOrigin: true,
        },
      },
    },
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
  resolve: {
    alias: [
      {
        find: '@',
        replacement: resolve(__dirname, 'src')
      }
    ]
  },
})
