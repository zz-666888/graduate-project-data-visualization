import request from "@/uitls/request";

/**
 * 登录接口
 */
export const postUserLogin = (data:{
  username: string,
  password: string
}) => {
  return request<{items: API.IUserLogin[], token: string}>('/api/login', {
     method: 'post',
     headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    }).then((data) => data)
}