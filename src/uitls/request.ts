/**
 * 封装 fetch 请求
 */
import { message } from 'ant-design-vue'

function request<T>(url: string, config: RequestInit): Promise<T>{
  
  return fetch(url, {
    credentials: 'include',
    ...config,
  }).then((response)=>{
    if(!response.ok){
      message.error(response.statusText || '请求失败')
    }
    return response.json() as Promise<{
        data: T
        code: number
        error?: { message: string }
    }>
  }).then((responseData)=>{
    if(responseData.code !== 200){
      message.error(responseData.error?.message || '请求失败')
    }
    return responseData.data
  })
}

export default request