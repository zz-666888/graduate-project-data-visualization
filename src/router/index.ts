import type { RouteRecordRaw } from 'vue-router'
import {createRouter, createWebHistory} from 'vue-router'

const Login = () => import('@/views/login/index.vue')
const Dashboard = () => import('@/views/dashboard/index.vue')
const routes = [
  {
    path:'/login',
    // redirect: '/login',
    component: Login,
    children: []
  },
  {
      path:'/dashboard',
      component: Dashboard,
    children: []
    }
] as RouteRecordRaw[]
 
export default createRouter({
  history: createWebHistory(),
  routes,
})